# Flatpak Common Lisp

This is a BuildStream project for building Flatpak based runtime
environments for Common Lisp applications.


## Installation

You have to use my BuildStream workspace since the elements require
features not yet available in an official BuildStream release.  So,
if ‘~bst’ names the top-level directory of the BuildStream workspace,
then clone this repository into the ‘~bst/projects’ directory.

    $ cd ~bst/projects
    $ git clone https://gitlab.com/ralph-schleicher/flatpak-common-lisp.git
    $ cd flatpak-common-lisp

Download the files BuildStream can't handle itself.

    $ make -C downloads


## Example

Build the full Commong Lisp runtime environment.

    $ bst build cl.bst

Examine it.

    $ bst shell cl.bst
    [e636d26e@cl.bst:/]$ ccl
    Clozure Common Lisp Version 1.12  LinuxX8664

    For more information about CCL, please see http://ccl.clozure.com.

    CCL is free software.  It is distributed under the terms of the Apache
    Licence, Version 2.0.
    ? (quit)
    [e636d26e@cl.bst:/]$ sbcl
    This is SBCL 2.0.11, an implementation of ANSI Common Lisp.
    More information about SBCL is available at <http://www.sbcl.org/>.

    SBCL is free software, provided as is, with absolutely no warranty.
    It is mostly in the public domain; some portions are provided under
    BSD-style licenses.  See the CREDITS and COPYING files in the
    distribution for more information.
    * (quit)
    [e636d26e@cl.bst:/]$ exit

That's it so far.


## Quicklisp

Quicklisp will be required to build Lisp applications.  Since there is
no network access from a build sandbox, all Quicklisp archives have to
be fetched via BuildStream.  Quicklisp itself has to be installed on a
writable file system.  Inside a build sandbox only the `/tmp`
directory is writable.  Thus, the top-level working directory for
Quicklisp (or any other Common Lisp build system) will be
`/tmp/common-lisp`.
